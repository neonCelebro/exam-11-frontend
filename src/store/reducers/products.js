import {CREATE_PRODUCTS_ERROR, FETCH_PRODUCT_SUCCESS, FETCH_PRODUCTS_SUCCESS} from "../actions/actionTypes";

const initialState = {
    products: {},
    product: null,
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products};
        case CREATE_PRODUCTS_ERROR:
            return {...state, error: action.error};
        case FETCH_PRODUCT_SUCCESS:
            return {...state, product: action.product};
        default:
            return state;
    }
};

export default reducer;
import axios from '../../axios-api';
import {CREATE_PRODUCTS_ERROR, CREATE_PRODUCTS_SUCCESS, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCT_SUCCESS} from "./actionTypes";
import {push} from "react-router-redux";

export const fetchProductsSuccess = products => {
  return {type: FETCH_PRODUCTS_SUCCESS, products};
};

export const createProductSuccess = () => {
  return {type: CREATE_PRODUCTS_SUCCESS};
};

export const createProductError = (error) => {
    return {type: CREATE_PRODUCTS_ERROR, error};
};

export const fetchProductSuccess = (product) => {
    return {type: FETCH_PRODUCT_SUCCESS, product};
};

export const fetchProducts = (category) => {
    return dispatch => {
        axios.get(category? `/products/${category}` : '/products').then(
            response => dispatch(fetchProductsSuccess(response.data))
        );
    }
};

export const fetchProduct = (id) => {
    return dispatch => {
        axios.get(`/products/product/${id}`).then(
            response => dispatch(fetchProductSuccess(response.data))
        );
    }
};

export const createProduct = productData => {
  return dispatch => {
    return axios.post('/products', productData).then(
      response => dispatch(createProductSuccess()),
        error => dispatch(createProductError(error.data))
    );
  };
};

export const deleteProduct = id => {
    return dispatch => {
        return axios.delete(`/products/product/${id}`).then(
            response => dispatch(push('/')),
            error => dispatch(console.log(error.data))
        );
    };
};
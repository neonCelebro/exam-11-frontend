import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteProduct, fetchProduct} from "../store/actions/products";
import Panel from "react-bootstrap/es/Panel";
import ListGroup from "react-bootstrap/es/ListGroup";
import ListGroupItem from "react-bootstrap/es/ListGroupItem";
import {Button, Image} from "react-bootstrap";

const url = "http://localhost:8000/uploads/";

class Product extends Component{

    componentDidMount(){
        this.props.fetchProduct(this.props.match.params.id);
    }

    render(){
        return (
            this.props.product?
                <Panel>
                <Panel.Heading>{this.props.product.title}</Panel.Heading>
                <ListGroupItem>Category: {this.props.product.category.title}</ListGroupItem>
                <Panel.Body>{this.props.product.description}</Panel.Body>
                <ListGroup>
                    <ListGroupItem><Image src={url + this.props.product.image}/></ListGroupItem>
                    <ListGroupItem>Price: {this.props.product.price}</ListGroupItem>
                    <ListGroupItem>&hellip;</ListGroupItem>
                    <ListGroupItem>Имя Прозовца: {this.props.product.userID.displayName}</ListGroupItem>
                    <ListGroupItem>Телефон Прозовца: {this.props.product.userID.phoneNumber}</ListGroupItem>
                </ListGroup>
                {this.props.user && this.props.user._id === this.props.product.userID._id?
                    <Button onClick={() => this.props.deleteProduct(this.props.product._id)} bsStyle='danger'>Delete product</Button> : null}
            </Panel> : <p>product not found</p>
            )
    };
}

const mapStateToProps = state => {
    return {
        product: state.products.product,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchProduct: id => dispatch(fetchProduct(id)),
        deleteProduct: id => dispatch(deleteProduct(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
import React from 'react';
import {Nav, NavItem} from "react-bootstrap";

const NavCategory = props => {
    return (<Nav
            style={{magrin: '20px'}}
            bsStyle="tabs"
            activeKey="1"
        >
            <NavItem
                onClick={() => props.onClick()}
            >
                Все товары
            </NavItem>
            {props.category?
                props.category.map((category, id) => {
                return(
                    <NavItem
                        onClick={() => props.onClick(category._id)}
                        key={id}
                    >
                        {category.title}
                </NavItem>
                )
            }): null}
    </Nav>
    )};

export default NavCategory;


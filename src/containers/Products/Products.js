import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {fetchProducts} from "../../store/actions/products";
import {Link} from "react-router-dom";

import ProductListItem from '../../components/ProductListItem/ProductListItem';
import NavCategory from "../../components/UI/Toolbar/Menus/NavCategory";

class Products extends Component {
    componentDidMount() {
        this.props.onFetchProducts();
    }

    render() {
        return (
            <Fragment>
                <NavCategory onClick={this.props.onFetchProducts} category={this.props.products.category}/>
                {this.props.user? <PageHeader>
                Products
                <Link to="/products/new">
                    <Button bsStyle="primary" className="pull-right">
                        Add product
                    </Button>
                </Link>
            </PageHeader> : null}
                {this.props.products.products?
                    this.props.products.products.map(product => (
                    <ProductListItem
                        key={product._id}
                        id={product._id}
                        title={product.title}
                        price={product.price}
                        image={product.image}
                    />
                )): null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.products.products,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProducts: (categoryID) => dispatch(fetchProducts(categoryID))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct} from "../../store/actions/products";
import {Redirect} from "react-router-dom";

class NewProduct extends Component {

  createProduct = productData => {
    this.props.onProductCreated(productData).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    if (!this.props.user) return <Redirect to='/login'/>;
    return (
      <Fragment>
        <PageHeader>New product</PageHeader>
        <ProductForm user={this.props.user._id} category={this.props.category} onSubmit={this.createProduct} />
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    category: state.products.products.category,
      user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onProductCreated: productData => {
      return dispatch(createProduct(productData))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);